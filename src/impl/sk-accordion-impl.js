

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';


export class SkAccordionImpl extends SkComponentImpl {

    get suffix() {
        return 'accordion';
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    restoreState(state) {
        this.contentEl.innerHTML = '';
        this.contentEl.insertAdjacentHTML('beforeend', state.contentsState);
        this.renderTabs();
        this.bindTabSwitch();
    }

    afterRendered() {
        super.afterRendered();
        this.clearAllElCache();
        this.restoreState({ contentsState: this.contentsState || this.comp.contentsState });
        this.mountStyles();
    }

    bindTabSwitch() {
        this.contentEl.querySelectorAll('.accordion-header').forEach(function(link) {
            link.onclick = function(event) {
                this.comp.callPluginHook('onEventStart', event);
                if (this.comp.getAttribute('disabled')) {
                    return false;
                }
                let target = event.currentTarget;
                let tabId = target.getAttribute('data-tab');
                this.toggleTab(tabId);
                this.comp.callPluginHook('onEventEnd', event);
            }.bind(this);
        }.bind(this));
    }

    closeAllTabs() {
        let tabs = this.contentEl.querySelectorAll('.accordion-header');
        for (let tab of tabs) {
            let tabNum = tab.dataset.tab;
            this.closeTab(tabNum);
        }
    }

    closeTab(tabNum) {
        let tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
            tab.removeAttribute('open');
            let activeTab = this.contentEl.querySelector(`#header-${tabNum}`);
            activeTab.classList.remove('accordion-header-active', 'state-active');

            let activeTabPane = this.contentEl.querySelector(`#panel-${tabNum}`);
            activeTabPane.style.display = 'none';
            this.selectedTab = null;
        }
    }

    toggleTab(tabNum) {
        let tab = this.tabs['tabs-' + tabNum];
        if (tab.hasAttribute('open')) {
            this.closeTab(tabNum);
        } else {
            if (this.comp.hasAttribute('mono')) {
                if (this.selectedTab) {
                    this.closeTab(this.selectedTab);
                } else {
                    this.closeAllTabs();
                }
            }
            let activeTab = this.contentEl.querySelector(`#header-${tabNum}`);
            activeTab.classList.add('accordion-header-active', 'state-active');

            let activeTabPane = this.contentEl.querySelector(`#panel-${tabNum}`);
            activeTabPane.style.display = 'block';
            this.selectedTab = tabNum;
            tab.setAttribute('open', '');
        }
    }
}