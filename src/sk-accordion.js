
import { SkElement } from '../../sk-core/src/sk-element.js';


export class SkAccordion extends SkElement {

    get cnSuffix() {
        return 'accordion';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }

    set impl(impl) {
        this._impl = impl;
    }

}
